package Calculator;

/**
 * Created by LENOVO on 5/23/2016.
 */
public class Evaluator {
    public static double Checker(String Operand1, String Operand2, String Operator){
        double x = Double.parseDouble(Operand2);
        double y = Double.parseDouble(Operand1);
        double z = 0;
        if (Operator.equals("+")) {
            z = x + y;
        }
        if (Operator.equals("-")) {
            z = x - y;
        }
        if (Operator.equals("/")) {
            z = x / y;
        }
        if (Operator.equals("*")) {
            z = x * y;
        }
        return z;
    }
    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    public static String evaluate(char[]postfix) {
        Stacks A = new Stacks();
        String infixString = "";
        for (int i = 0; i < postfix.length; i++) {
            infixString += "" + postfix[i];
        }
        String infixSpaces = "";
        for (int i = 0; i < infixString.length(); i++) {
            if (infixString.charAt(i) == '-' || infixString.charAt(i) == '+' || infixString.charAt(i) == '/' || infixString.charAt(i) == '*' || infixString.charAt(i) == '(' || infixString.charAt(i) == ')') {
                infixSpaces += " " + infixString.charAt(i) + " ";
            } else {
                infixSpaces += infixString.charAt(i);
            }
        }
        String[] tokenString = infixSpaces.split(" ");
        for (int i = 0; i < tokenString.length; i++) {
            String Operand1 = "";
            String Operand2 = "";
            if (isDouble(tokenString[i])) {
                A.push(tokenString[i]);
            }
            if (tokenString[i].equals("+") || tokenString[i].equals("-") || tokenString[i].equals("*") || tokenString[i].equals("/")) {
                if (!A.isEmpty()) {
                    Operand1 = A.pop();
                    Operand2 = A.pop();
                    if (Operand1 != null && Operand2 != null){
                        A.push("" + (Checker(Operand1, Operand2, tokenString[i])));}
                    else{
                        System.out.println("Invalid Expression");
                        break;
                    }
                } else {
                    System.out.println("Invalid Expression");
                    break;
                }

            }
        }
        return A.pop();

    }

}
