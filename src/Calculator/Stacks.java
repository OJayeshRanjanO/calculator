package Calculator;

/**
 * Created by LENOVO on 5/23/2016.
 */
public class Stacks {
    StackNode top;
    public class StackNode{
        String data;
        StackNode next;
        StackNode(String data){
            this.data = data;
            this.next = null;
        }
    }
    public void push(String element){
        if (top == null){
            top = new StackNode(element);
        }
        else{
            StackNode previousTop = top;
            top = new StackNode(element);
            top.next = previousTop;
        }
    }

    public String pop(){
        if (top == null){
            return "Stack is empty";
        }
        else{
            StackNode temp = top;
            top = top.next;
            return temp.data;
        }
    }
    public String peek(){
        if (top == null){
            return "Stack is empty";
        }
        else{
            return top.data;
        }
    }
    public boolean isEmpty(){
        if (top == null){
            return true;
        }
        else{
            return false;
        }
    }
    public String printStack(){
        StackNode temp = top;
        String s = "";
        while(temp!= null){
            s += temp.data + " ";
            temp = temp.next;

        }
        return s;
    }
}
