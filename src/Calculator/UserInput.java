package Calculator;

import Calculator.Converter;
import Calculator.Evaluator;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class UserInput extends Application {
    String exp = "";

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        Evaluator e = new Evaluator();
        Converter c = new Converter();

        primaryStage.setTitle("Calculator");

        GridPane layout = new GridPane();
        layout.setPadding(new Insets(10, 10, 10, 10)); //Sets 10px padding on the sides
        layout.setVgap(10);
        layout.setHgap(10);

        //GridPane
        TextField expression = new TextField();
        Button seven = new Button("7");
        Button eight = new Button("8");
        Button nine = new Button("9");
        Button four = new Button("4");
        Button five = new Button("5");
        Button six = new Button("6");
        Button one = new Button("1");
        Button two = new Button("2");
        Button three = new Button("3");
        Button divide = new Button("/");
        Button multiply = new Button("*");
        Button add = new Button("+");
        Button subtract = new Button("-");
        Button leftBracket = new Button("(");
        Button rightBracket = new Button(")");
        Button enterButton = new Button("Enter");
        Button Erase = new Button("Erase");
        Button Clear = new Button("Clear");


        expression.setPromptText("Expression to evaluate");
        expression.setStyle("-fx-text-fill: blue;");
        GridPane.setConstraints(expression, 2, 1, 5, 2);//Allowing flexible sizes
        expression.setPadding(new Insets(15, 15, 15, 15));

        expression.setDisable(true);//To disable external inputs

        GridPane.setConstraints(seven, 2, 5);
        seven.setPadding(new Insets(20, 20, 20, 20));
        seven.setOnAction(a -> {
            String N7 = "7";
            expression.setText(concat(N7));

        });
        GridPane.setConstraints(eight, 3, 5);
        eight.setPadding(new Insets(20, 20, 20, 20));
        eight.setOnAction(a -> {
            String N8 = "8";
            expression.setText(concat(N8));

        });

        GridPane.setConstraints(nine, 4, 5);
        nine.setPadding(new Insets(20, 20, 20, 20));
        nine.setOnAction(a -> {
            String N9 = "9";
            expression.setText(concat(N9));

        });

        GridPane.setConstraints(four, 2, 6);
        four.setPadding(new Insets(20, 20, 20, 20));
        four.setOnAction(a -> {
            String N4 = "4";
            expression.setText(concat(N4));

        });

        GridPane.setConstraints(five, 3, 6);
        five.setPadding(new Insets(20, 20, 20, 20));
        five.setOnAction(a -> {
            String N5 = "5";
            expression.setText(concat(N5));

        });

        GridPane.setConstraints(six, 4, 6);
        six.setPadding(new Insets(20, 20, 20, 20));
        six.setOnAction(a -> {
            String N6 = "6";
            expression.setText(concat(N6));

        });

        GridPane.setConstraints(one, 2, 7);
        one.setPadding(new Insets(20, 20, 20, 20));
        one.setOnAction(a -> {
            String N1 = "1";
            expression.setText(concat(N1));

        });

        GridPane.setConstraints(two, 3, 7);
        two.setPadding(new Insets(20, 20, 20, 20));
        two.setOnAction(a -> {
            String N2 = "2";
            expression.setText(concat(N2));

        });

        GridPane.setConstraints(three, 4, 7);
        three.setPadding(new Insets(20, 20, 20, 20));
        three.setOnAction(a -> {
            String N3 = "3";
            expression.setText(concat(N3));

        });

        GridPane.setConstraints(divide, 5, 5);
        divide.setPadding(new Insets(20, 20, 20, 20));
        divide.setOnAction(a -> {
            String ND = "/";
            expression.setText(concat(ND));

        });

        GridPane.setConstraints(multiply, 6, 5);
        multiply.setPadding(new Insets(20, 20, 20, 20));
        multiply.setOnAction(a -> {
            String NM = "*";
            expression.setText(concat(NM));

        });

        GridPane.setConstraints(add, 5, 6);
        add.setPadding(new Insets(20, 17, 20, 18));
        add.setOnAction(a -> {
            String NA = "+";
            expression.setText(concat(NA));

        });

        GridPane.setConstraints(subtract, 6, 6);
        subtract.setPadding(new Insets(20, 20, 20, 20));
        subtract.setOnAction(a -> {
            String NS = "-";
            expression.setText(concat(NS));

        });

        GridPane.setConstraints(leftBracket, 5, 7);
        leftBracket.setPadding(new Insets(20, 20, 20, 20));
        leftBracket.setOnAction(a -> {
            String NL = "(";
            expression.setText(concat(NL));

        });

        GridPane.setConstraints(rightBracket, 6, 7);
        rightBracket.setPadding(new Insets(20, 20, 20, 20));
        rightBracket.setOnAction(a -> {
            String NR = ")";
            expression.setText(concat(NR));

        });

        GridPane.setConstraints(Erase, 5, 8);
        Erase.setPadding(new Insets(20, 6, 20, 5));
        Erase.setOnAction(a -> {
            if (!exp.equals("")) {
                String NE = exp.substring(0, exp.length() - 1);
                exp = "";
                expression.setText(concat(NE));
            }

        });

        GridPane.setConstraints(Clear, 6, 8);
        Clear.setPadding(new Insets(20, 6, 20, 5));
        Clear.setOnAction(a -> {
            if (!exp.equals(null)) {
                exp = "";
                expression.setText(concat(exp));
            }


        });

        GridPane.setConstraints(enterButton, 2, 8, 3, 1); // For flexible size, so that row,columns don't resize (Since Enter button spans over 1 column but less than 2 columns)
        enterButton.setMinWidth(167);
        enterButton.setPadding(new Insets(20, 20, 20, 20));
        enterButton.setOnAction(f -> {
            if (!exp.equals("")) {
                String answer = e.evaluate((c.convert(expression.getText().toCharArray())).toCharArray());
                expression.setText("Answer: " + answer);
            }
        });


        layout.getChildren().addAll(expression, enterButton, seven, eight, nine, five, four, three, two, one, six, divide, multiply, add, subtract, leftBracket, rightBracket, Erase, Clear);//Adding things to the layout
        Scene scene = new Scene(layout, 340, 400); //Setting the scene
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    public String concat(String s) {
        exp += s;
        return exp;
    }
}
