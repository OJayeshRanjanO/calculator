package Calculator;

/**
 * Created by LENOVO on 5/23/2016.
 */
public class Converter {
    public boolean precedence(String topOp, String token) {
        int precedenceToken = 0, precedenceTopOp = 0;
        if (token.equals("$")) {
            precedenceToken = 0;
        } else if (token.equals("(")) {
            precedenceToken = 1;
        } else if ((token.equals("-") || token.equals("+"))) {
            precedenceToken = 2;
        } else if ((token.equals("/") || token.equals("*"))) {
            precedenceToken = 3;
        }
        if (topOp.equals("$")) {
            precedenceTopOp = 0;
        } else if (topOp.equals("(")) {
            precedenceTopOp = 1;
        } else if (topOp.equals("-") || topOp.equals("+")) {
            precedenceTopOp = 2;
        } else if (topOp.equals("/") || topOp.equals("*")) {
            precedenceTopOp = 3;
        }
        return precedenceTopOp >= precedenceToken;
    }

    public boolean isDouble(String a) {
        try {
            Double.parseDouble(a);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public String convert(char[] infix) {
        String infixString = "";
        for (int i = 0; i < infix.length; i++) {
            infixString += "" + infix[i];
        }
        String infixSpaces = "";
        for (int i = 0; i < infixString.length(); i++) {
            if (infixString.charAt(i) == '-' || infixString.charAt(i) == '+' || infixString.charAt(i) == '/' || infixString.charAt(i) == '*' || infixString.charAt(i) == '(' || infixString.charAt(i) == ')') {
                infixSpaces += " " + infixString.charAt(i) + " ";
            } else {
                infixSpaces += infixString.charAt(i);
            }
        }
        String[] tokenString = infixSpaces.split(" ");

        Stacks A = new Stacks();
        Stacks b = new Stacks();
        String P = " ";
        String topOp = "";
        String token = "";
        for (int i = 0; i < tokenString.length; i++) {

            if (isDouble(tokenString[i])) {
                P += tokenString[i] + " ";
            } else if (tokenString[i].equals("(")) {
                A.push(tokenString[i]);
            } else if (tokenString[i].equals(")")) {
                while (!A.peek().equals("(")) {
                    P += A.peek() + " ";
                    A.pop();
                }
                A.pop();
            } else if (tokenString[i].equals("+") || tokenString[i].equals("-") || tokenString[i].equals("/") || tokenString[i].equals("*")) {
                while (!A.isEmpty() && precedence(A.peek(), tokenString[i]) && !A.peek().equals("(")) {
                    P += A.pop() + " ";
                }
                ;
                A.push(tokenString[i]);
            }
        }
        while (!A.isEmpty()) {
            P = P + A.pop() + " ";

        }
        if (P.equals("")) {
            return "";
        }
        else{
            return P;}
    }
}
